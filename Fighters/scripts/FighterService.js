import {callApi} from './callApi.js'

export default class FighterService {
  async getFighters() {
    try {
      const endpoint = 'repos/sahanr/street-fighter/contents/fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }
  async getFighterInfo(id){   
    
    
    try {
      const endpoint = 'repos/sahanr/street-fighter/contents/fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      const fighter = JSON.parse(atob(apiResult.content))
     
      return fighter.find((item)=> item._id === id)
      
     
    } catch (error) {
      throw error;
    }
    }
}