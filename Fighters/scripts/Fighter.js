export default class Fighter {
    constructor({_id, name, source}, health, attack, defense){ 
        this._id = _id       
        this.name = name;  
        this.soure = source      
        this.health = health;
        this.attack = attack;        
        this.defense = defense;
       
    }
     get getHitPower(){        
       return this.attack *Math.ceil(Math.random() *2)
    }

     get getBlockPower(){        
     return this.defense * Math.ceil(Math.random() *2)
    }
}