import FighterService from './FighterService.js'
import FightersView from './FightersView.js'
import Fighter from './Fighter.js'
const fighterService = new FighterService();



export default class App {
    constructor() {
      this.startApp();
    }
  
    static rootElement = document.getElementById('root');
    static loadingElement = document.getElementById('loading-overlay');
    
    async startApp() {
      try {
        App.loadingElement.style.visibility = 'visible';
        
        const fighters = await fighterService.getFighters();


        const fighterInfo = await fighterService.getFighterInfo('1')
        console.log(fighterInfo);

        const fighterCharacters = new Fighter(fighterInfo, 200, 15, 20)
        console.log(fighterCharacters)
        
        console.log(`Hit Power = ${fighterCharacters.getHitPower}`)
        console.log(`Block Power ${fighterCharacters.getBlockPower}`)

        const fightersView = new FightersView(fighters);
        const fightersElement = fightersView.element;
  
        App.rootElement.appendChild(fightersElement);
        
        
        

      } catch (error) {
        console.warn(error);
        App.rootElement.innerText = 'Failed to load data';
      } finally {
        App.loadingElement.style.visibility = 'hidden';
      }
    }
  }
  
 