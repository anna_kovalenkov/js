const puppeteer = require('puppeteer');
describe('Pop The bubble', () => {
    beforeEach(() => {
        jest.setTimeout(20000);
    })

    test('Pop The bubble', async () => {
        const browser = await puppeteer.launch({headless: false})
        const page = await browser.newPage()
        await page.goto('https://task1-bvckdxdkxw.now.sh')
        await page.waitFor(5000)
        const result = await page.evaluate(() => {
            let elements = document.querySelectorAll('.bubble');
            return elements.length
        });

        for(let i=0; i<result; i++){
            await page.click('.bubble')
        }

        const score = await page.evaluate(() => {
            let elements = document.getElementById('score');
            return +elements.innerText
        });
        browser.close()

        expect(score).toBe(result)

    });
});
