const puppeteer = require('puppeteer');


describe('Pop The Infinity bubble', () => {
    beforeEach(() => {
        jest.setTimeout(2147483647)
    })


    test('Pop The Infinity bubble', async () => {

        const browser = await puppeteer.launch({headless: false})
        const page = await browser.newPage()
        await page.goto('https://task1-bvckdxdkxw.now.sh')

        while(1){
            const result = await page.evaluate(() => {
                let elements = document.querySelectorAll('.bubble');
                return elements.length
            });
            let oldScore = 0
            if(result>0){
                await page.click('.bubble')
                const score = await page.evaluate(() => {
                    let elements = document.getElementById('score');
                    return elements.innerText
                });
                expect(+score).toBeGreaterThan(oldScore);
                oldScore = +score
            }
        }
    });
});
